import { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {projectFireStore} from '../../firebase/config'
//import { useFetch } from "../../hooks/useFetch";

//Styles
import './Create.css';

export default function Create() {

    const [title, setTitle] = useState('');
    const [method, setMethod] = useState('');
    const [cookingTime, setCookingTime] = useState('');
    const [newIngredient, setNewIngredient] = useState('');
    const [ingredients, setingredients] = useState([]);
    const ingredientInput = useRef(null);
    const history = useHistory()

    //const {postData, data, error} = useFetch('http://localhost:3000/recipes', 'POST')

    const handleSubmit = async (e)=>{
        e.preventDefault()

        console.log();
        const doc = {title, method, cookingTime: cookingTime + ' minutes', ingredients}
        history.push('/')
        try {
            await projectFireStore.collection('recipes').add(doc)   
        } catch (err) {
            console.log(err)
        }

    }

    const handleAdd = (e)=>{
        e.preventDefault();
        const ing = newIngredient.trim()

        if (ing && !ingredients.includes(ing)) {
            setingredients(prevIngredients => [...prevIngredients, ing])
        }

        setNewIngredient('')
        ingredientInput.current.focus()
    }

    return (
        <div className='create'>
            <h2 className="page-title">Add a New Recipe</h2>
            <form onSubmit={handleSubmit}>
                <label>
                    <span>Recipe Title:</span>
                    <input type='text' onChange={(e) => setTitle(e.target.value)} value={title} required></input>
                </label>


                {/* ingrediants go here */}
                <label>
                    <span>Recipe Ingrediants:</span>
                    <div className="ingredients">
                        <input type='text' onChange={(e) => setNewIngredient(e.target.value)} value={newIngredient} ref={ingredientInput}></input>
                        <button className='btn' onClick={handleAdd}>Add</button>
                    </div>
                </label>

                <label>
                    <span>Recipe Method:</span>
                    <textarea onChange={(e) => setMethod(e.target.value)} value={method} required/>
                </label>

                <label>
                    <span>Cooking time (minutes):</span>
                    <input type='number' onChange={(e)=> setCookingTime(e.target.value)} value={cookingTime} required></input>
                </label>

                <p>Current Ingredients : {ingredients.map(i => <em key={i}>{i}, </em>)}</p>

                <button className='btn'>Submit</button>
            </form>
        </div>
    )
}