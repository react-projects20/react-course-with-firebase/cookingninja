import {projectFireStore} from '../../firebase/config'
import { useEffect, useState } from 'react';
//import { useFetch } from "../../hooks/useFetch";

//Components
import RecipeList from "../../components/RecipeList";

//Styles
import './Home.css'


export default function Home() {

    const [data, setData] = useState(null)
    const [isPending ,setIsPending] = useState(false)
    const [error, setError] = useState(false)

    useEffect(()=>{
        setIsPending(true)
        const unsub = projectFireStore.collection('recipes').onSnapshot((snapshot)=>{
            
            if (snapshot.empty) {
                setError('No recipes to load')
                setIsPending(false)
            } else{
                let results = []
                snapshot.docs.forEach(doc => {
                    results.push({id: doc.id, ...doc.data()})
                    //console.log(doc)
                })
                setData(results)
                setIsPending(false)
            }
        }, (err)=> {
            setError(err.message)
            setIsPending(false)
        })

        return ()=> unsub()

    }, [])

    return (
        <div className="home">
            {error && <p className="error">{error}</p>}
            {isPending && <p className="loading">Loading...</p>}
            {data && <RecipeList recipes={data}></RecipeList>}
        </div>
    )
}