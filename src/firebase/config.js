import firebase from 'firebase/app';
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyCNBwvi4TpSljsLDW3lNiPcUKbGgnDWJ0Y",
    authDomain: "cooking-ninja-site-65cbc.firebaseapp.com",
    projectId: "cooking-ninja-site-65cbc",
    storageBucket: "cooking-ninja-site-65cbc.appspot.com",
    messagingSenderId: "613960560842",
    appId: "1:613960560842:web:158d03f7a7b4b39a63c433"
  };

  //initalize firebase
  firebase.initializeApp(firebaseConfig)

  //initialize services
  const projectFireStore = firebase.firestore()

  export {projectFireStore}